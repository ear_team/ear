/*********************************************************************
 * Copyright (c) 2024 Energy Aware Solutions, S.L
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

#ifndef ENERGY_NODE_CPU_GPU_H
#define ENERGY_NODE_CPU_GPU_H
#include <metrics/gpu/gpu.h>
#include <metrics/energy_cpu/energy_cpu.h>

#endif
