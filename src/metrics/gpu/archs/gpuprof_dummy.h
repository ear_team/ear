/***************************************************************************
 * Copyright (c) 2024 Energy Aware Runtime - Barcelona Supercomputing Center
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **************************************************************************/

#ifndef METRICS_GPUPROF_DUMMY_H
#define METRICS_GPUPROF_DUMMY_H

#include <metrics/gpu/gpuprof.h>

GPUPROF_DEFINES(dummy);

#endif //METRICS_GPUPROF_DCGMI_H