/***************************************************************************
 * Copyright (c) 2024 Energy Aware Runtime - Barcelona Supercomputing Center
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **************************************************************************/


#ifndef _EARL_METRICS_FINE_GRAIN_H
#define _EARL_METRICS_FINE_GRAIN_H


#include <common/states.h>


state_t fine_grain_metrics_init();


#endif // _EARL_METRICS_FINE_GRAIN_H
