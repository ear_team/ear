/***************************************************************************
 * Copyright (c) 2024 Energy Aware Runtime - Barcelona Supercomputing Center
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **************************************************************************/

#ifndef APP_CLASSIFICATION_LIMITS
#define APP_CLASSIFICATION_LIMITS


#define CPI_CPU_BOUND_DEF         0.6
#define GBS_CPU_BOUND_DEF         30.0
#define CPI_BUSY_WAITING_DEF      0.4
#define GBS_BUSY_WAITING_DEF      3.0
#define GFLOPS_BUSY_WAITING_DEF   0.1
#define IO_TH_DEF                 10.0
#define CPI_MEM_BOUND_DEF         0.8
#define GBS_MEM_BOUND_DEF         100.0

#endif // APP_CLASSIFICATION_LIMITS
