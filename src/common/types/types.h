/***************************************************************************
 * Copyright (c) 2024 Energy Aware Runtime - Barcelona Supercomputing Center
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **************************************************************************/

#ifndef EAR_TYPES_H
#define EAR_TYPES_H

#include <common/types/log.h>
#include <common/types/loop.h>
#include <common/types/generic.h>
#include <common/types/application.h>
#include <common/types/periodic_metric.h>
#include <common/types/periodic_aggregation.h>
#include <common/types/configuration/cluster_conf.h>

#include <common/types/job.h>
#include <common/types/loop.h>
#include <common/types/services.h>
#include <common/types/pc_app_info.h>

#endif //EAR_TYPES_H
