/***************************************************************************
 * Copyright (c) 2024 Energy Aware Runtime - Barcelona Supercomputing Center
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **************************************************************************/

#ifndef COMMON_CONFIG_H
#define COMMON_CONFIG_H

#include <common/config/config_def.h>
#include <common/config/config_dev.h>
#include <common/config/config_env.h>
#include <common/config/config_install.h>

#endif
