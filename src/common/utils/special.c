/***************************************************************************
 * Copyright (c) 2024 Energy Aware Runtime - Barcelona Supercomputing Center
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **************************************************************************/

#include <common/utils/special.h>

char *special_ear_logo()
{
    char *logo = "    _________    ____ \n"
                 "   / ____/   |  / __ \\\n"
                 "  / __/ / /| | / /_/ /\n"
                 " / /___/ ___ |/ _, _/ \n"
                 "/_____/_/  |_/_/ |_|  \n";
    return logo;
}