/***************************************************************************
 * Copyright (c) 2024 Energy Aware Runtime - Barcelona Supercomputing Center
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **************************************************************************/

#ifndef UP_MANAGEMENT_H
#define UP_MANAGEMENT_H

#include <management/management.h>
#include <common/system/plugin_manager.h>

typedef struct mans_s {
    manages_info_t mi;
} mans_t;

#endif //UP_MANAGEMENT_H